import './App.css';
import Form from "./components/Form/Form";
import {useEffect, useState} from "react";
import axios from "axios";
import Message from "./components/Message/Message";

const App = () => {
  const [message, setMessage] = useState({
      message: '',
      author: ''
    }
  );

  const url = 'http://146.185.154.90:8000/messages';

  const sendMessage = async (e) => {
    e.preventDefault();

    const data = new URLSearchParams();
    data.set('message', message.message);
    data.set('author', message.author);

    try {
      await axios.post(url, data)
    } catch (error) {
      console.log(e);
    }
  };

  const onChange = e => {
    const {value, name} = e;

    setMessage(prev => ({
      ...prev,
      [name]: value
    }));
  };

  const [messages, setMessages] = useState([]);

  useEffect(() => {
    const getMessages = async () => {
      try {
        const response = await axios.get(url)
        const msgs = response.data;

        setMessages(msgs);

        let datetime = msgs[msgs.length - 1]['datetime'];

        setInterval(async () => {
          const lastPost = await axios.get(url + '?datetime=' + datetime);

          if (lastPost.data.length !== 0) {
            datetime = lastPost.data[0]['datetime'];

          }
        }, 3000);
      } catch (e) {
        console.log('Error', e);
      }
    }

    getMessages().catch(e => console.error(e));
  }, [messages]);

  return (
    <div className="container">
      <div className="content">
        <Form
          onSub={sendMessage}
          message={message.message}
          author={message.author}
          onInputChange={onChange}
        />
        {messages.map(msg => (
          <Message
            key={msg._id}
            author={msg.author}
            message={msg.message}
          />
        ))}
      </div>
    </div>
  );
};

export default App;
