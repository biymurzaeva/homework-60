import React from 'react';
import './Form.css';

const Form = props => {
	return (
		<div className="form-block">
			<form onSubmit={props.onSub}>
				<div className="form-row">
					<label htmlFor="message" className="form-label">Message: </label>
					<input
						type="text"
						name="message"
						id="message"
						value={props.message}
						onChange={e => props.onInputChange(e.target)}/>
				</div>
				<div className="form-row">
					<label htmlFor="author" className="form-label">Author: </label>
					<input
						type="text"
						name="author"
						id="author"
						value={props.author}
						onChange={e => props.onInputChange(e.target)}/>
				</div>
				<button type="submit">Send</button>
			</form>
		</div>
	);
};

export default Form;