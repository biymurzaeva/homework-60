import React from 'react';
import './Message.css';

const Message = props => {
	return (
		<div className="message-block">
			<h4>{props.author} said:</h4>
			<p>{props.message}</p>
		</div>
	);
};

export default Message;